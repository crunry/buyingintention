
# coding: utf-8

# In[2]:


import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
from matplotlib.pyplot import figure, show
import seaborn as sns
from __future__ import division
from sklearn.preprocessing import LabelEncoder
from sklearn.preprocessing import OneHotEncoder
from numpy import array
from numpy import argmax
from scipy.stats import zscore


# In[3]:


data = pd.read_csv("C:/Users/Babe-PC/kerjaan_hadi/buyingintention/problem1/DATA.csv", sep=";")
data.head()

(total_instances, total_features) = data.shape
print "total instance: " + str(total_instances)
print "total fitur: " + str(total_features)


# In[4]:


#visualisasi distribusi frekuensi hanya untuk variabel dengan tipe kategorikal
data_plot = data.iloc[:,[0, 1, 2, 3, 11, 12, 13, 15, 16, 17, 19, 22, 26, 27, 29]]
for col in data_plot:
    values = data_plot[col].value_counts(sort=False)
    names = values.keys()
    plt.bar(range(len(names)), values)
    plt.xticks(range(len(values)), names)
    plt.xlabel(col)
    plt.ylabel("frequency")
    plt.savefig('{}.png'.format(col))
    plt.show()


# Dengan melihat visualisasi distribusi di atas, didapatkan bahwa:
# 1. Data yang diolah tidak seimbang, yaitu data yang diklasifikasikan sebagai class 0 jauh lebih banyak dibandingkan dengan class 1
# 2. Terdapat beberapa kategori yang imbalanced seperti variabel platform, condition, varUser_1, varUser_2, varUser_3, varSeller_1, varSeller_2, varSeller_3, varSeller_5, varProduct_3, varProduct_4, dan varGeneral_1

# In[5]:


#mengambil data dengan class = 1
data_class_1 = data[data.response == 1]
data_class_1.head()


# In[6]:


#eksplorasi data kategorikal
sns.set_style("whitegrid")
data_bar_plot = data.iloc[:,[1, 2, 3, 11, 12, 13, 15, 16, 17, 19, 22, 26, 27, 29]]
for col in data_bar_plot:
    countplot = sns.countplot(x=col, hue="response", data=data_class_1)
    show()


# In[7]:


for col in data_bar_plot:
    count_series = data_class_1.groupby(['response', col]).size()
    print count_series


# In[8]:


for col in data_bar_plot:
    count_series = data_class_1.groupby(['response', col]).size()
    print max(count_series)


# In[9]:


mode_categorical_name = data.category_name.mode()
print mode_categorical_name


# Dengan melihat grafik dan perhitungan di atas, didapatkan bahwa dari data sampai pengguna memutuskan untuk membeli, yaitu:
# 1. Platform yang paling banyak digunakan adalah apps
# 2. Kategori yang dilihat oleh user dan paling banyak dibeli adalah 262
# 3. Kondisi barang yang paling banyak dibeli adalah baru

# In[10]:


#eksplorasi data numerik
data_hist = data.iloc[:,[4, 5, 6, 7, 8, 9, 10, 14, 18, 20, 21, 23, 24, 25, 28]]
for col in data_hist:
    data_class_1[col].hist(bins='sturges')
    plt.xlabel(col)
    plt.ylabel("frequency")
    plt.savefig('{}-HC.png'.format(col))
    plt.show()


# Dilihat dari kumpulan histogram di atas, didapatkan bahwa barang dengan:
# 1. jumlah stok dalam range kurang dari 5000 paling banyak dibeli
# 2. jumlah area dengan pengiriman barang gratis dalam range kurang dari 5 paling banyak dibeli
# 3. jumlah pelayanan kurir yang tersedia dalam range antara 2 sampai 5 paling banyak dibeli
# 4. jumlah view barang dalam range kurang dari 500 paling banyak dibeli
# 5. waktu sampai barang sampai ke pengguna dalam range 2 sampai 4 paling banyak dibeli
# 6. banyak pengguna favoritkan dalam range kurang dari 500 paling banyak dibeli
# 7. jumlah terjual dalam range kurang dari 500 paling banyak dibeli

# In[11]:


#menghitung proporsi data berdasarkan response
count_zero_response = len(data[data["response"] == 0])
count_one_response = len(data_class_1)
percentage_zero_response = count_zero_response / total_instances
percentage_one_response = count_one_response / total_instances
print "percentage of zero response is " + str(percentage_zero_response * 100)
print "percentage of one response is " + str(percentage_one_response * 100)


# In[12]:


#melakukan undersampling berdasarkan sumber berikut: https://www.kaggle.com/gargmanish/how-to-handle-imbalance-data-study-in-detail

one_indices = np.array(data[data.response==1].index)
zero_indices = np.array(data[data.response==0].index)


# In[13]:


def undersample(zero_indices, one_indices):
    random_indices = np.random.choice(zero_indices, len(data[data['response'] == 1]), replace=False)
    under_sample_indices = np.concatenate([one_indices,random_indices])
    under_sample = data.loc[under_sample_indices]
    return under_sample


# In[14]:


undersample_data = undersample(zero_indices, one_indices)
(total_instance_sample, feature_sample) = undersample_data.shape
print "the zero response proportion is :" + str(len(undersample_data[undersample_data.response==0])/total_instance_sample)
print "the one response proportion is :" + str(len(undersample_data[undersample_data.response==1])/total_instance_sample)
print "total number of record in resampled data is:" + str(total_instance_sample)


# Jumlah instance pada sampel sebanyak 7896, sedangkan jumlah instance pada data secara keseluruhan yaitu 259347. Dari perhitungan dengan menggunakan kalkulator https://www.checkmarket.com/sample-size-calculator/, didapatkan margin of error sebesar 1.43% dengan confidence level 99%. Dengan kata lain, data sampel yang digunakan cukup baik untuk mewakiloi data secara keseluruhan

# In[15]:


print "MEAN DATA"
print "------------------------------------"
print data.mean()
print "MEAN UNDER SAMPLE DATA"
print "------------------------------------"
print undersample_data.mean()
print "MEDIAN DATA"
print "------------------------------------"
print data.median()
print "MEDIAN UNDER SAMPLE DATA"
print "------------------------------------"
print undersample_data.median()
print "STANDAR DEVIATION DATA"
print "------------------------------------"
print data.std()
print "STANDAR DEVIATION UNDER SAMPLE DATA"
print "------------------------------------"
print undersample_data.std()


# Dari hasil di atas, didapatkan bahwa hasil pengukuran statistik antara sampling data dengan data secara keseluruhan (mean, median, dan standar deviasi) hampir sama. 

# In[16]:


missing_value_total = sum(data.isnull().sum())
print data.isnull().sum()
print missing_value_total


# Dari perhitungan di atas, didapatkan bahwa banyak missing values yang ditemukan pada data secara keseluruhan, yaitu 124212. 

# In[17]:


missing_value_total = sum(undersample_data.isnull().sum())
print undersample_data.isnull().sum()
print missing_value_total


# Dari perhitungan di atas, didapatkan bahwa banyak missing values yang ditemukan pada sampling data, yaitu 3074.

# In[18]:


mean_seller_deliv_resp_time = undersample_data['seller_delivery_response_time'].mean()
median_seller_deliv_resp_time = undersample_data['seller_delivery_response_time'].median()
std_seller_deliv_resp_time = undersample_data['seller_delivery_response_time'].std()
cv_seller_deliv_resp_time = std_seller_deliv_resp_time / mean_seller_deliv_resp_time
print "rata-rata atribut seller_delivery_response_time: " + str(mean_seller_deliv_resp_time)
print "median atribut seller_delivery_response_time: " + str(median_seller_deliv_resp_time)
print "standar deviasi atribut seller_delivery_response_time: " + str(std_seller_deliv_resp_time)
print "coefficient of variation atribut seller_delivery_response_time: " + str(cv_seller_deliv_resp_time)
mean_varSeller_6 = undersample_data['varSeller_6'].mean()
median_varSeller_6 = undersample_data['varSeller_6'].median()
std_varSeller_6 = undersample_data['varSeller_6'].median()
cv_varSeller_6 = std_varSeller_6 / mean_varSeller_6
print "rata-rata atribut seller_delivery_response_time: " + str(mean_varSeller_6)
print "median atribut seller_delivery_response_time: " + str(median_varSeller_6)
print "standar deviasi atribut varSeller_6: " + str(std_varSeller_6)
print "coefficient of variation atribut varSeller_6: " + str(cv_varSeller_6)
mean_varSeller_7 = undersample_data['varSeller_7'].mean()
median_varSeller_7 = undersample_data['varSeller_7'].median()
std_varSeller_7 = undersample_data['varSeller_7'].std()
cv_varSeller_7 = std_varSeller_7 / mean_varSeller_7
print "rata-rata atribut seller_delivery_response_time: " + str(mean_varSeller_7)
print "median atribut seller_delivery_response_time: " + str(median_varSeller_7)
print "standar deviasi atribut varSeller_7: " + str(std_varSeller_7)
print "coefficient of variation atribut varSeller_7: " + str(cv_varSeller_7)


# Ketiga atribut di atas merupakan atribut dengan tipe numerik dan memiliki missing values. Berdasarkan nilai coefficient of variation dari ketiga atribut tersebut, maka:
# 1. digunakan mean sebagai pengisi nilai missing values atribut seller_delivery_response_time dan varSeller_6
# 2. digunakan median sebagai pengisi nilai missing values atribut varSeller_7
# 
# Atribut dengan tipe kategorikal dan memiliki missing values, yaitu varUser_1 dan varSeller_8. Untuk atribut-atribut tersebut akan digunakan modus sebagai pengisi nilai missing values.

# In[19]:


#mengisi missing values
undersample_data['seller_delivery_response_time'] = undersample_data['seller_delivery_response_time'].fillna(undersample_data['seller_delivery_response_time'].mean())
undersample_data['varSeller_6'] = undersample_data['varSeller_6'].fillna(undersample_data['varSeller_6'].mean())
undersample_data['varSeller_7'] = undersample_data['varSeller_7'].fillna(undersample_data['varSeller_6'].median())

mode_1 = undersample_data['varUser_1'].mode()
mode_8 = undersample_data['varSeller_8'].mode()
print mode_1
print mode_8
undersample_data['varUser_1'] = undersample_data['varUser_1'].fillna(mode_1[0])
undersample_data['varSeller_8'] = undersample_data['varSeller_8'].fillna(mode_8[0])


# In[20]:


undersample_data.isnull().sum()


# In[22]:


#normalisasi data
list_column_num = list(data_hist.columns)

undersample_data_scaled = undersample_data[list_column_num].apply(zscore)
undersample_data_scaled.head()
for col in undersample_data_scaled:
    y = range(len(undersample_data_scaled[col]))
    x = undersample_data_scaled[col]
    mean = undersample_data_scaled[col].mean()
    std = undersample_data_scaled[col].std()
    plt.bar(y,x)
    plt.xlabel(col)
    plt.plot((0, 8000), (mean, mean), 'g-')
    plt.plot((0, 8000), (mean+2*std,mean+2*std), 'b-')
    plt.plot((0, 8000), (mean-2*std, mean-2*std), 'r-')
    plt.savefig('{}-ZScore.png'.format(col))
    plt.show()


# Berdasarkan visualisasi z-score di atas, maka atribut dengan tipe numerik yang memiliki tidak memiliki outlier adalah varProduk_5, sisanya mengandung outlier. Jika diperhatikan kembali, kebanyakan datapoint yang outlier memiliki jarak yang tidak terlalu jauh dari standar deviasi, sehingga masih bisa ditoleransi sebagai bukan outlier.

# In[26]:


list_column_cat = list(data_bar_plot.columns)
undersample_data_category = undersample_data[list_column_cat]

undersample_data_final = pd.concat([undersample_data_scaled, undersample_data_category, undersample_data['response']], axis=1)
undersample_data_final.head()


# In[27]:


undersample_data_final.to_csv("undersample_data_final.csv")


# In[28]:


undersample_data_final.corr(method='pearson').style.format("{:.2}").background_gradient(cmap=plt
.get_cmap('coolwarm'), axis=1)


# Berdasarkan gambar di atas, didapatkan bahwa:
# 1. favorite_count memiliki correlation strength +1 dengan varProduct_1. Keduanya bertipe numerik.
# 2. varSeller_9 memiliki correlation strength +1 dengan varSeller_4. Keduanya bertipe numerik.
# 3. varSeller_1 memiliki correlation strength +0.99 dengan varUser_1. Keduanya bertipe kategorikal dan memiliki nilai unik yang sama.
# 
# Dengan kata lain: atribut favorite_count redundan dengan atribut varProduct_1, atribut varSeller_9 redundan dengan varSeller_4, dan varSeller_1 redundan dengan varUser_1
